﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{

    private float Speed;

    // Start is called before the first frame update
    void Start()
    {
        Speed = 4.0f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += -transform.up * Speed * Time.deltaTime;
    }
}
