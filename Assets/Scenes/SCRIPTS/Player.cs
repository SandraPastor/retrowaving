﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public GameObject win;
    public GameObject lose;
    private Touch touch;
    private float Speed;

    public AudioSource WinSound;
    public AudioSource LoseSound;


    // Start is called before the first frame update
    void Start()
    {
        Speed = 0.0075f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3(
                    transform.position.x + touch.deltaPosition.x * Speed,
                    transform.position.y,
                    transform.position.z + touch.deltaPosition.y * Speed);
            }
        }
    }

    // Collision interaction with obstacles
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            Destroy(gameObject);
            LoseSound.Play();
            lose.SetActive(true);
            if (Time.timeScale == 1f)
            {
                Time.timeScale = 0f;
            }
        }
        else
        {
            lose.SetActive(false);
            
        }

        if (collision.gameObject.tag == "Finish")
        {
            win.SetActive(true);
            WinSound.Play();
            if (Time.timeScale == 1f)
            {
                Time.timeScale = 0f;
            }
        }
        else
        {
            win.SetActive(false);
            
        }

    }
}
