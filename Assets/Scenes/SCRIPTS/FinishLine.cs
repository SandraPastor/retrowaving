﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{

    private float Speed;

    // Start is called before the first frame update
    void Start()
    {
        Speed = 9.5f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * Speed * Time.deltaTime, Space.World);
    }
}
