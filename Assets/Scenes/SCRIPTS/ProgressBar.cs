﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{

    public Transform levelStart;
    public Transform levelEnd;
    public Transform player;

    public Slider slider;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // get the distances
        var maxDistance = Vector3.Distance(levelStart.position, levelEnd.position);
        var currentDistance = Vector3.Distance(levelStart.position, player.position);

        // then set the slider value (clamping the value between 0 and 1)
        slider.value = Mathf.Clamp01(currentDistance / maxDistance);
    }
}
