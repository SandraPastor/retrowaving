﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject Pause;
    public AudioSource Music;
    private float MusicVolume = 1f;
    public AudioSource WinSound, LoseSound;
    private float WinVolume = 1f;
    private float LoseVolume = 1f;

    public GameObject Ship;

    void Update()
    {
       
        Music.volume = MusicVolume;
        WinSound.volume = WinVolume;
        LoseSound.volume = LoseVolume;
    }

    public void UpdateVolume (float volume)
    {
        MusicVolume = volume;
    }
    public void UpdateSFXVolume(float volume)
    {
        WinVolume = volume;
        LoseVolume = volume;
    }
    public void PauseMenuFunction()
    {
       if (Time.timeScale == 1f)
       {
           Time.timeScale = 0f;
       }
       Pause.SetActive(true);
       Ship.SetActive(false);
    }

    public void ResumeMenuFunction()
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
        }
        Pause.SetActive(false);
        Ship.SetActive(true);
    }

    public void BacktoMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menú");
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Game");
    }
}
